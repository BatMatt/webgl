# WebGL

Mini project made with THREEJs

Un travail un peu maigre, et je l'avoue, fait dans l'urgence. J'ai essayé d'utiliser un Raycaster et des Vector afin de gérer des événements de click sur des Mesh.
Je souhaitais aller plus loin, mais je m'y suis vraiment mis tard. Lors d'un click sur un Mesh (ayant la forme d'un bouton), la couleur du bouton change de manière aléatoire.
Un commit unique, mais pour la démarche de conception, j'ai appelé une méthod OnClick crée dans ma Scene sur un événement onclick javascript.

J'ai d'abord compris comment l'interaction fonctionne avec un événement hover. Afin d'identifier mes boutons, je leur ai donné un nom.
Puis j'ai bouclé sur le tableau d'objet de ma scene et lorsque mon bouton se trouvait en première position du tableau (soit le plus proche à ce que j'ai compris), alors j'ai changé la couleur de l'objet.
J'espère avoir été clair.

Je lance le projet grâce au Live Server de VSCode.

En vous souhaitant de bonnes fêtes, encore toutes mes excuses pour mon maigre effort sur ce projet.
