import * as THREE from './vendor/three.js-master/build/three.module.js';
import Stats from './vendor/three.js-master/examples/jsm/libs/stats.module.js';
import { OrbitControls } from './vendor/three.js-master/examples/jsm/controls/OrbitControls.js';
import { FBXLoader } from './vendor/three.js-master/examples/jsm/loaders/FBXLoader.js';

var texture = new THREE.TextureLoader().load('texture/marbre.jpg');

const Scene = {
    vars: {
        container: null,
        scene: null,
        renderer: null,
        camera: null,
        stats: null,
        controls: null
    },
    animate: () => {
        Scene.render();
        requestAnimationFrame(Scene.animate);
    },
    render: () => {

        Scene.vars.renderer.render(Scene.vars.scene, Scene.vars.camera);
        Scene.vars.stats.update();
    },
    loadFBX: (file, scale, position, rotation, color, namespace, callback) => {
        let vars = Scene.vars;
        let loader = new FBXLoader();

        if (file === undefined) {
            return;
        }

        loader.load('./fbx/' + file, (object) => {

            object.traverse((child) => {
                if (child.isMesh) {
                    if (namespace === "plaquette") {
                        child.material = new THREE.MeshBasicMaterial({
                            map: texture,
                        }
                        )
                    } else if (namespace === "statuetteGold") {
                        child.material = new THREE.MeshStandardMaterial({
                            roughness: 0.3,
                            metalness: 0.6
                        }
                        )
                    }
                    child.material.color = new THREE.Color(color);
                    child.castShadow = true;
                    child.receiveShadow = true;
                }
            });

            object.position.x = position[0];
            object.position.y = position[1];
            object.position.z = position[2];

            object.rotation.x = rotation[0];
            object.rotation.y = rotation[1];
            object.rotation.z = rotation[2];

            object.scale.x = object.scale.y = object.scale.z = scale;


            Scene.vars[namespace] = object;


            callback();
        });

    },
    onWindowResize: () => {
        let vars = Scene.vars;
        vars.camera.aspect = window.innerWidth / window.innerHeight;
        vars.camera.updateProjectionMatrix();
        vars.renderer.setSize(window.innerWidth, window.innerHeight);
    },
    onClick: (event) => {
        let vars = Scene.vars;

        event.preventDefault();

        var mouseVector = new THREE.Vector3(
            ( event.clientX / window.innerWidth ) * 2 - 1,
          - ( event.clientY / window.innerHeight ) * 2 + 1,
            1 );
    
        mouseVector.unproject(vars.camera );
        var raycaster = new THREE.Raycaster( vars.camera.position, mouseVector.sub( vars.camera.position ).normalize() );
    
        // create an array containing all objects in the scene with which the ray intersects
        var intersects = raycaster.intersectObjects( vars.scene.children );
        for (let i = 0; i<intersects.length; i++){
            if (intersects[0].object.name === 'cubeGold') {
                intersects[ 0 ].object.material.color.setHex( Math.random() * 0xffffff );
            }

            if (intersects[0].object.name === 'cubeSilver') {
                intersects[ 0 ].object.material.color.setHex( Math.random() * 0xffffff );
            }

            if (intersects[0].object.name === 'cubeBronze') {
                intersects[ 0 ].object.material.color.setHex( Math.random() * 0xffffff );
            }
        }
    },

    init: () => {
        let vars = Scene.vars;

        // Préparer le container pour la scène
        vars.container = document.createElement('div');
        vars.container.classList.add('fullscreen');
        document.body.appendChild(vars.container);

        // ajout de la scène
        vars.scene = new THREE.Scene();
        vars.scene.background = new THREE.Color(0xa0a0a0);

        // paramétrage du moteur de rendu
        vars.renderer = new THREE.WebGLRenderer({ antialias: true });
        vars.renderer.shadowMap.enabled = true;
        vars.renderer.shadowMapSoft = true;
        vars.renderer.setPixelRatio(window.devicePixelRatio);
        vars.renderer.setSize(window.innerWidth, window.innerHeight);
        vars.container.appendChild(vars.renderer.domElement);

        // ajout de la caméra
        vars.camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 2000);
        vars.camera.position.set(-1.5, 210, 572);

        // ajout de la lumière
        const lightIntensityHemisphere = 0.5;
        let light = new THREE.HemisphereLight(0xFFFFFF, 0x444444, lightIntensityHemisphere);
        light.position.set(0, 700, 0);
        vars.scene.add(light);

        let mesh = new THREE.Mesh(
            new THREE.PlaneBufferGeometry(2000, 2000),
            new THREE.MeshLambertMaterial(
                { color: new THREE.Color(0x888888) }
            )
        );
        mesh.rotation.x = -Math.PI / 2;
        mesh.receiveShadow = false;
        vars.scene.add(mesh);

        let grid = new THREE.GridHelper(2000, 20, 0x000000, 0x000000);
        grid.material.opacity = 0.2;
        grid.material.transparent = true;
        grid.receiveShadow = false;
        vars.scene.add(grid);

        let geometry = new THREE.SphereGeometry(1000, 32, 32);
        let material = new THREE.MeshPhongMaterial({ color: new THREE.Color(0xFFFFFF) });
        material.side = THREE.DoubleSide;
        let sphere = new THREE.Mesh(geometry, material);
        vars.scene.add(sphere);

        let planeMaterial = new THREE.ShadowMaterial();
        planeMaterial.opacity = 0.07;
        let shadowPlane = new THREE.Mesh(new THREE.PlaneBufferGeometry(2000,
            2000), planeMaterial);
        shadowPlane.rotation.x = -Math.PI / 2;
        shadowPlane.receiveShadow = true;
        vars.scene.add(shadowPlane);

        var geoBtnGold = new THREE.BoxGeometry(40, 20, 20);
        var matBtnGold = new THREE.MeshBasicMaterial({ color: 0xff0000 });
        var cube = new THREE.Mesh(geoBtnGold, matBtnGold);
        cube.name = 'cubeGold';
        cube.position.y = 30;
        cube.position.z = 0;
        vars.scene.add(cube);

        var geoBtnSilver = new THREE.BoxGeometry(40, 20, 20);
        var matBtnSilver = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
        var cube = new THREE.Mesh(geoBtnSilver, matBtnSilver);
        cube.name = 'cubeSilver';
        cube.position.x = -165;
        cube.position.z = 55;
        cube.position.y = 30;
        cube.rotation.y = Math.PI / 4;
        vars.scene.add(cube);

        var getBtnBronze = new THREE.BoxGeometry(40, 20, 20);
        var matBtnBronze = new THREE.MeshBasicMaterial({ color: 0x0000ff });
        var cube = new THREE.Mesh(getBtnBronze, matBtnBronze);
        cube.name = 'cubeBronze';
        cube.position.x = 160;
        cube.position.z = 60;
        cube.position.y = 30;
        cube.rotation.y = -Math.PI / 4;
        vars.scene.add(cube);




        Scene.loadFBX("Logo_Feelity.FBX", 10, [45, 20, 0], [0, 0, 0], 0xFFFFFF, "logo1", () => {
            Scene.loadFBX("Plaquette.FBX", 10, [0, 0, 45], [0, 0, 0], 0xFFFFFF, "plaquette", () => {
                Scene.loadFBX("Socle_Partie1.FBX", 10, [0, 0, 0], [0, Math.PI, 0], 0x484747, "socle1", () => {
                    Scene.loadFBX("Socle_Partie2.FBX", 10, [0, 0, 0], [0, Math.PI, 0], 0x484747, "socle2", () => {
                        Scene.loadFBX("Statuette.FBX", 10, [0, 0, 0], [0, 0, 0], 0xFFFF00, "statuetteGold", () => {

                            let vars = Scene.vars;

                            let logo2 = vars.logo1.clone();
                            logo2.position.x = -45;
                            logo2.position.y = 20;
                            logo2.rotation.z = Math.PI;


                            let gold = new THREE.Group();
                            gold.add(vars.logo1);
                            gold.add(logo2);
                            gold.add(vars.plaquette);
                            gold.add(vars.socle1);
                            gold.add(vars.socle2);
                            gold.add(vars.statuetteGold);
                            vars.scene.add(gold);

                            gold.position.z = -50;
                            gold.position.y = 10;
                            vars.goldGroup = gold;

                            let silver = gold.clone();
                            silver.position.x = -200;
                            silver.position.z = 20;
                            silver.position.y = 10;
                            silver.rotation.y = Math.PI / 4;
                            silver.children[5].traverse(node => {
                                if (node.isMesh) {
                                    node.material = new THREE.MeshStandardMaterial({
                                        color: new THREE.Color(0xC0C0C0)
                                    })
                                }
                            });
                            vars.scene.add(silver);
                            vars.silverGroup = silver;

                            let bronze = gold.clone();
                            bronze.position.x = 200;
                            bronze.position.z = 20;
                            bronze.position.y = 10;
                            bronze.rotation.y = -Math.PI / 4;
                            bronze.children[5].traverse(node => {
                                if (node.isMesh) {
                                    node.material = new THREE.MeshStandardMaterial({
                                        color: new THREE.Color(0xDF320)
                                    })
                                }
                            });
                            vars.scene.add(bronze);
                            vars.bronzeGroup = bronze;

                            let elem = document.querySelector('#loading');
                            elem.parentNode.removeChild(elem);
                        });
                    });
                });
            });
        });

        var lights = [{
            "lightIntensity": 1,
            "color": 0xffffff,
            "size": 5,
            "x": 0,
            "y": 200,
            "z": 200,
            "target": {
                "x": 0,
                "y": 10,
                "z": -50
            }
        }, {
            "lightIntensity": 0.5,
            "color": 0x0000FF,
            "size": 5,
            "x": 100,
            "y": 100,
            "z": 100,
            "target": {
                "x": -200,
                "y": 20,
                "z": 10
            }
        }, {
            "lightIntensity": 0.5,
            "color": 0x0000FF,
            "size": 5,
            "x": -100,
            "y": 100,
            "z": 100,
            "target": {
                "x": 200,
                "y": 20,
                "z": 10
            }
        },
        {
            "lightIntensity": 1,
            "color": 0xF879FF,
            "size": 10,
            "x": 0,
            "y": 500,
            "z": 400,
            "target": {
                "x": 0,
                "y": 20,
                "z": 10
            }
        }];

        lights.forEach((light) => {
            let lightIntensity = light.lightIntensity;
            let directional = new THREE.DirectionalLight(light.color, lightIntensity);

            directional.position.set(light.x, light.y, light.z);
            directional.target.position.set(light.target.x, light.target.y, light.target.z);

            directional.castShadow = true;
            let d = 1000;
            directional.shadow.camera.left = -d;
            directional.shadow.camera.right = d;
            directional.shadow.camera.top = d;
            directional.shadow.camera.bottom = -d;
            directional.shadow.camera.far = 2000;
            directional.shadow.mapSize.width = 4096;
            directional.shadow.mapSize.height = 4096;

            vars.scene.add(directional.target);
            vars.scene.add(directional);
            let helper = new THREE.DirectionalLightHelper(directional, light.size);
            vars.scene.add(helper);
        });

        vars.controls = new OrbitControls(vars.camera, vars.renderer.domElement);
        vars.controls.target.set(0, 100, 0);
        vars.controls.update();

        window.addEventListener('resize', Scene.onWindowResize, false);

        window.addEventListener('click', Scene.onClick, false);

        vars.stats = new Stats();
        vars.container.appendChild(vars.stats.dom);

        Scene.animate();
    }
};

Scene.init();
